import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Consumer;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.*;

/**
 * Created by rdas on 16/05/2016.
 */
public class PhotoMetadataTest {

    @Test
    public void readMetadata() throws IOException {
        Files.walk(Paths.get("/Users/rdas/Pictures/Shanghai-2016/")).forEach(filePath -> {
            if (Files.isRegularFile(filePath)) {
                System.out.println(filePath);
                try {
                    File jpegFile = new File(filePath.toString());
                    Metadata metadata = ImageMetadataReader.readMetadata(jpegFile);
                    for (Directory directory : metadata.getDirectories()) {
                        for (Tag tag : directory.getTags()) {
                            System.out.println(tag);
                        }
                    }
                } catch (IOException | ImageProcessingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Ignore
    public void runAndDisplayMetsdata() throws IOException {
        //FileFilterGoodies.listRecursive(new File("~/Pictures/Shanghai-2016/"));
        Files.walk(Paths.get("/Users/rdas/Pictures/Shanghai-2016/")).forEach(filePath -> {
            if (Files.isRegularFile(filePath)) {
                System.out.println(filePath);
                try {
                    readAndDisplayMetadata(filePath.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void readAndDisplayMetadata(String fileName) throws IOException {

        File file = new File(fileName);
        ImageInputStream iis = ImageIO.createImageInputStream(file);
        Iterator<ImageReader> readers = ImageIO.getImageReaders(iis);

        if (readers.hasNext()) {

            // pick the first available ImageReader
            ImageReader reader = readers.next();

            // attach source to the reader
            reader.setInput(iis, true);

            // read metadata of first image
            IIOMetadata metadata = reader.getImageMetadata(0);

            String[] names = metadata.getMetadataFormatNames();
            int length = names.length;
            for (int i = 0; i < length; i++) {
                System.out.println("Format name: " + names[i]);
                displayMetadata(metadata.getAsTree(names[i]));
            }
        }
    }

    void displayMetadata(Node root) {
        displayMetadata(root, 0);
    }

    void indent(int level) {
        for (int i = 0; i < level; i++)
            System.out.print("    ");
    }

    void displayMetadata(Node node, int level) {
        // print open tag of element
        indent(level);
        System.out.print("<" + node.getNodeName());
        NamedNodeMap map = node.getAttributes();
        if (map != null) {

            // print attribute values
            int length = map.getLength();
            for (int i = 0; i < length; i++) {
                Node attr = map.item(i);
                System.out.print(" " + attr.getNodeName() +
                        "=\"" + attr.getNodeValue() + "\"");
            }
        }

        Node child = node.getFirstChild();
        if (child == null) {
            // no children, so close element and return
            System.out.println("/>");
            return;
        }

        // children, so close current tag
        System.out.println(">");
        while (child != null) {
            // print children recursively
            displayMetadata(child, level + 1);
            child = child.getNextSibling();
        }

        // print close tag of element
        indent(level);
        System.out.println("</" + node.getNodeName() + ">");
    }
    ///////

    public static class FileFilterGoodies {

//        public static void main(String args[]) {
//            listRecursive(new File("."));
//        }

        /**
         * This method recursively lists all
         * .txt and .java files in a directory
         */
        private static void listRecursive(File dir) {
            Arrays.stream(dir.listFiles((f, n) ->
                    !n.startsWith(".")
                            &&
                            (new File(f, n).isDirectory()
                                    || n.endsWith(".txt")
                                    || n.endsWith(".java"))
            ))
                    .forEach(unchecked((file) -> {
                        System.out.println(
                                file.getCanonicalPath()
                                        .substring(new File(".")
                                                .getCanonicalPath()
                                                .length()));

                        if (file.isDirectory()) {
                            listRecursive(file);
                        }
                    }));
        }

        /**
         * This utility simply wraps a functional
         * interface that throws a checked exception
         * into a Java 8 Consumer
         */
        private static <T> Consumer<T> unchecked(CheckedConsumer<T> consumer) {
            return t -> {
                try {
                    consumer.accept(t);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            };
        }

        @FunctionalInterface
        private interface CheckedConsumer<T> {
            void accept(T t) throws Exception;
        }
    }
}
