package com.rdas.lmax.util;

import com.lmax.disruptor.EventFactory;
import com.rdas.lmax.event.WriteEvent;

/**
 * Created by rdas on 28/11/2015.
 */
public class WriteEventFactory implements EventFactory<WriteEvent> {

    @Override
    public WriteEvent newInstance() {
        return new WriteEvent();
    }
}
