package com.rdas.lmax.util;

import com.lmax.disruptor.EventHandler;
import com.rdas.lmax.event.WriteEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by rdas on 28/11/2015.
 */
@Slf4j
public class WriteEventHandler implements EventHandler<WriteEvent> {

    @Override
    public void onEvent(WriteEvent writeEvent, long l, boolean b) throws Exception {
        if (writeEvent != null && StringUtils.isNotEmpty(writeEvent.getMessage())) {
            String message = writeEvent.getMessage();
            // Put you business logic here.
            // In this first example, it will print only the submitted message.
            log.info(message + " processed.");
        }
    }
}
