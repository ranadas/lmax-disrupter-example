package com.rdas.lmax.event;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by rdas on 28/11/2015.
 */
public class WriteEvent {
    @Getter @Setter
    private String message;
}
