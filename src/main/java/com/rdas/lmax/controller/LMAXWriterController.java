package com.rdas.lmax.controller;

import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.dsl.Disruptor;
import com.rdas.lmax.event.WriteEvent;
import com.rdas.lmax.producer.WriteEventProducer;
import com.rdas.lmax.runner.WriteExceptionHandler;
import com.rdas.lmax.util.WriteEventFactory;
import com.rdas.lmax.util.WriteEventHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * A class to control the disruptor engine. It initialize the disruptor,
 * submits messages to event producers and closes the disruptor.
 * Created by rdas on 29/11/2015.
 */
@Slf4j
public class LMAXWriterController {
    private Disruptor<WriteEvent> disruptor;
    private WriteEventProducer writeEventProducer;

    private int ringBufferSize;

    private LMAXWriterController(){}

    public LMAXWriterController(int ringBufferSize) {
        if (ringBufferSize == 0) {
            ringBufferSize = 1024;
        }
        // ring buffer size always has to be the power of 2.
        // so if it is not, make it equal to the nearest integer.
        double power = Math.log(ringBufferSize) / Math.log(2);
        if (power % 1 != 0) {
            power = Math.ceil(power);
            ringBufferSize = (int) Math.pow(2, power);
            log.info("New ring buffer size = " + ringBufferSize);
        }

        this.ringBufferSize = ringBufferSize;
    }

    public void close() {
        if (disruptor != null) {
            disruptor.halt();
            disruptor.shutdown();
        }
    }

    public void submitMessage(String message) {
        if (writeEventProducer != null) {
            // publish the messages via event producer
            writeEventProducer.onData(message);
        }
    }

    /**
     * Initialize the disruptor engine.
     */
    public void init() {
        // create a thread pool executor to be used by disruptor
        Executor executor = Executors.newCachedThreadPool();

        // initialize our event factory
        WriteEventFactory factory = new WriteEventFactory();

        // initialize our event handler.
        WriteEventHandler handler = new WriteEventHandler();

        // initialize the disruptor
        disruptor = new Disruptor<WriteEvent>(factory, ringBufferSize, executor);
        disruptor.handleEventsWith(handler);

        // set our custom exception handler
        ExceptionHandler exceptionHandler = new WriteExceptionHandler();
        disruptor.handleExceptionsFor(handler).with(exceptionHandler);

        // start the disruptor and get the generated ring buffer instance
        disruptor.start();

        // initialize the event producer to submit messages
        writeEventProducer = new WriteEventProducer(disruptor);

        log.info("Disruptor engine started successfully.");
    }
}
