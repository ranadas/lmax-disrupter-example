package com.rdas.lmax.runner;

import com.google.common.base.Stopwatch;
import com.rdas.lmax.controller.LMAXWriterController;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * Created by rdas on 29/11/2015.
 */
@Slf4j
public class DisruptorApplication {

    public static void main(String[] args) {
        //deliberately set. Final ring buffer size would be 8.
        LMAXWriterController lmaxWriterController = new LMAXWriterController(7);
        log.info("Initializing lmax disruptor.");
        lmaxWriterController.init();


        log.debug("Generate message & Submit");
        // submit messages to write concurrently using disruptor
        Stopwatch stopwatch = Stopwatch.createStarted();
        IntStream.rangeClosed(0, 1000000).parallel().forEach(i ->
                lmaxWriterController.submitMessage(String.format("\t ***Generated Message Sequence (%d) ", i))
        );
        lmaxWriterController.close();

        stopwatch.stop();
        log.info("All message submitted, lmax disruptor closed. Process Took ({})", stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }
}
