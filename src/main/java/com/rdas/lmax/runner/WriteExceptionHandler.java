package com.rdas.lmax.runner;

import com.lmax.disruptor.ExceptionHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by rdas on 29/11/2015.
 */
@Slf4j
public class WriteExceptionHandler implements ExceptionHandler {
    public void handleEventException(Throwable throwable, long sequence, Object obj) {
        log.error("Exception while handling event.", throwable);
    }

    public void handleOnStartException(Throwable throwable) {
        log.error("Exception while starting up.", throwable);
    }

    public void handleOnShutdownException(Throwable throwable) {
        log.error("Exception while shutting down.", throwable);
    }
}
