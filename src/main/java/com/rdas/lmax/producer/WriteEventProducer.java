package com.rdas.lmax.producer;

import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.dsl.Disruptor;
import com.rdas.lmax.event.WriteEvent;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by rdas on 28/11/2015.
 */
@Slf4j
public class WriteEventProducer {

    private final Disruptor<WriteEvent> disruptor;

    public WriteEventProducer(Disruptor<WriteEvent> disruptor) {
        this.disruptor = disruptor;
    }

    private static final EventTranslatorOneArg<WriteEvent, String> TRANSLATOR_ONE_ARG =
            new EventTranslatorOneArg<WriteEvent, String>() {
                public void translateTo(WriteEvent writeEvent, long sequence, String message) {
                    log.debug("Inside translator");
                    writeEvent.setMessage(message);
                }
            };

    public void onData(String message) {
        log.info("Publishing message [{}]." , message);
        // publish the message to disruptor
        disruptor.publishEvent(TRANSLATOR_ONE_ARG, message);
    }
}
