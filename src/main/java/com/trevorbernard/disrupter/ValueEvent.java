package com.trevorbernard.disrupter;

import com.lmax.disruptor.EventFactory;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by rdas on 01/05/2017.
 */
@Getter
@Setter
public class ValueEvent {
    private String value;

    /*
    public final static EventFactory<ValueEvent> EVENT_FACTORY = new EventFactory<ValueEvent>() {
        public ValueEvent newInstance() {
            return new ValueEvent();
        }
    };
    */
    public final static EventFactory<ValueEvent> EVENT_FACTORY = () -> new ValueEvent();
}
