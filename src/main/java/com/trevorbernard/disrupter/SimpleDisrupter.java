package com.trevorbernard.disrupter;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

/**
 * https://github.com/LMAX-Exchange/disruptor/wiki/Getting-Started
 * Created by rdas on 01/05/2017.
 */
public class SimpleDisrupter {
    public static void main(String[] args) {

        ExecutorService executorService = Executors.newCachedThreadPool();
        // Pre-allocate RingBuffer with 1024 ValueEvents
        Disruptor<ValueEvent> disruptor = new Disruptor<>(ValueEvent.EVENT_FACTORY, 1024, executorService);
        // event will eventually be recycled by the Disruptor after it wraps
        final EventHandler<ValueEvent> handler = (event, sequence, endOfBatch) -> {
            System.out.println("Sequence: " + sequence);
            System.out.println("ValueEvent: " + event.getValue());
        };
        // Build dependency graph
        disruptor.handleEventsWith(handler);
        RingBuffer<ValueEvent> ringBuffer = disruptor.start();
        int endRange = 10;//00000;
        IntStream.rangeClosed(0, endRange).parallel().forEach(i ->
                {
                    String uuid = UUID.randomUUID().toString();
                    // Two phase commit. Grab one of the 1024 slots
                    long seq = ringBuffer.next();
                    ValueEvent valueEvent = ringBuffer.get(seq);
                    valueEvent.setValue(uuid);
                    ringBuffer.publish(seq);
                }
        );

        disruptor.shutdown();
        executorService.shutdown();
    }
}
