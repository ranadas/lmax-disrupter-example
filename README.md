The LMAX disruptor is a high performance inter-thread messaging library.
Low latency method of passing data between producers and consumers in a lock-free manner.


How to use LMAX Disruptor based on 
http://www.abstractclass.org/tutorial/java/2015/05/20/tutorial-how-to-use-lmax-disruptor.html


entry point : com.rdas.lmax.runner.DisruptorApplication


Read this : 

https://github.com/joragupra/disruptor/tree/master/src/main/java/com/joragupra/disruptorSample

http://mechanitis.blogspot.ie/2011/07/dissecting-disruptor-writing-to-ring.html